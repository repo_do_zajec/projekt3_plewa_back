const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const path = require('path');
const { formatujWiadomosc,
   addUzytkownik,
   getUzytkownik,
   delUzytkownik, } = require('./functions');
   
const config = {
   url: 'https://uni-call.fcc-online.pl',
   login: '<login>',
   password: 'haslo'
};

Dialer.configure(config);

app.use(cors());
app.use(bodyParser.json());


http.listen(3000, () => {
   console.log('app listening on port 3000');
});

//Dochodzi komunikacja po websocketach. W połączeniu aplikacji z frontendem websockety umożliwią nam bezpośrednie informowanie frontendu o statusie połączenia. Działanie socket-io jest dobrze opisane pod linkiem: https://socket.io/
io.on('connection', (socket) => {
   
   socket.on('uzytkownikDolaczyl', (uzytkownik) => {
      const user = addUzytkownik(socket.id, uzytkownik);
      socket.emit('wiadomosc', formatujWiadomosc(null, 'Witaj!'));
      socket.broadcast.emit('wiadomosc', formatujWiadomosc(null, 'Użytkownik ' + uzytkownik + ' dołączył do czatu'));
   });
   socket.on('chatMesseage', (msg) => {
      const user = getUzytkownik(socket.id);
      io.emit('wiadomosc', formatujWiadomosc(user.username, msg));
   });

   socket.on('disconnect', () => {
      const user = delUzytkownik(socket.id);
      if (user) {
         socket.broadcast.emit('wiadomosc', formatujWiadomosc(null, 'Użytkownik ' + user.username + ' opuścił czat'));
      }

   });
})

//Metoda POST - służąca do zestawienia połączenia.
app.post('/call', async (req, res) => {

   //Przypisujemy wartość "body" z parametrow zapytania. W obiekcie body znajdują się przesłane numery
   const body = req.body;

   //Wyświetlenie parametrów przekazanych w zapytaniu
   console.log(body);

   //Dialler.call to metoda z API, służąca do zestawienia połączenia. Jako argumenty przyjmuje dwa numery.

   bridge = await Dialer.call(body.number1, body.number2);
   let oldStatus = null

   // Rozpoczynamy interwał w ktorym co 500ms sprawdzany jest aktualny status połączenia. Jeśli status się zmienił to informujemy o tym po websockecie.
   let interval = setInterval(async () => {
      let currentStatus = await bridge.getStatus();
      if (currentStatus !== oldStatus) {
         oldStatus = currentStatus
         io.emit('message', currentStatus)
      }
   }, 500)

   res.json({ success: true });
})

