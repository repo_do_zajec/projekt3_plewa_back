const users = [];

function formatujWiadomosc(username, text) {
  return {
    username,
    text
  };
}

function addUzytkownik(id, username) {
  const user = { id, username };

  users.push(user);

  return user;
}

function getUzytkownik(id) {
  return users.find(user => user.id === id);
}

function delUzytkownik(id) {
  const index = users.findIndex(user => user.id === id);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
}


module.exports = {
  addUzytkownik,
  getUzytkownik,
  delUzytkownik,
  formatujWiadomosc
}; 